const io = require("socket.io-client");
const os = require('os');
const path = require("path");
const childProcess = require('child_process');
const electronPath  = require("electron");
const Store = require('electron-store')
const homeDir = os.homedir()

const store = new Store({
  cwd: homeDir,
  name: '.fslguiconf',
})

class TestDriver {
  constructor({path, args, env}) {
    env.FSL_GUI_MODE = 'test'
    this.isReady = () => new Promise(resolve => setTimeout(resolve, 5000));
    this.process = childProcess.spawn(path, args, { stdio: ['pipe', 'pipe', 'pipe', 'ipc'], env })
    this.process.stdout.on('data', (data) => {
      console.log(`FSLapp: ${data}`);
    });
  }

  stop() {
    this.process.kill()
  }
}

describe('FSL application', () => {
  let client

  beforeAll(async () => {
    if (process.env.ISCI !== 'true') {
      fslDriver = new TestDriver({
        path: electronPath, 
        args: ['./src/main.js', '--no-sandbox', '--disable-setuid-sandbox', '--enable-logging', '--ignore-gpu-blacklist'],
        env: {
          FSL_GUI_MODE: 'test'
        }
      })
      await fslDriver.isReady()
    }
    client = io(`ws://${store.get('host')}:${store.get('port-app')}`)
  })

  afterAll(() => {
    client.close()
    if (process.env.ISCI !== 'true') {
      fslDriver.stop()
    }
  })

  test('can select files', (done) => {
    client.on('files', (files) => {
      expect(files[0]).toBe('test_file.nii.gz')
      done()
    })
    client.emit('files')
  })
})