# FSL GUI Core Electron Application

This project contains the source code for the main FSL application (e.g. FSL.app on macOS). 

The deliverable is a native application for each supported platform (macOS, Linux, Windows). 

This application uses [Electron]() as a method to interact with the user's system. The main FSL application does not contain its own user interface, but rather acts as a hub to orchestrate all of the independent web-based FSL GUIs. 

This repo is responsible for building the main *FSL* app and the `fsl` command line entry point. The FSL app is the environment in which all other FSL GUIs run. Each FSL GUI is rendered in its own electron browser window, and must communicate with the main FSL electron application using the socket.io package (websockets or http long polling).

## Development

### Installation

You must have Node and NPM available on your system (the latest versions of each are recommended). 

```
# using https
git clone https://git.fmrib.ox.ac.uk/fsl/gui/fsl-gui-core.git

# using ssh 
git clone git@git.fmrib.ox.ac.uk:fsl/gui/fsl-gui-core.git

# cd into the project directory
cd fsl-gui-core

# install the dependencies using npm
npm install
```

### Run the electron app (during development) 

```
npm run start
```

### Build and package the entire application for distribution

```
npm run make
```

### `$FSLGUIDIR` - Set the path to look for installed GUIs (useful for testing)

`export FSLGUIDIR=/full/path/to/gui/dir`

A `$FSLGUIDIR` must resemble this basic directory hierarchy:

```
$FSLGUIDIR/
├── bet
│   ├── css
│   ├── index.html # required
│   ├── js
│   └── meta.json  # required
├── feat
│   ├── css
│   ├── index.html # required
│   ├── js
│   └── meta.json  # required
└── flirt
    ├── css
    ├── index.html # required
    ├── js
    └── meta.json  # required
```

only GUI folders containing a `meta.json` file will be available for users to activate.

### `$FSL_GUI_MODE` - Set the mode to run the GUI in (useful for testing)

options:

- test
- deploy

```
export FSL_GUI_MODE=test # useful for running automated tests`
export FSL_GUI_MODE=deploy # default mode when not testing
```

