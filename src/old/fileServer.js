const express = require('express');
const app = express()
const cors = require('cors')
const server = require('http').createServer(app);
const parser = require('minimist')
const os = require('os')
const FORK = process.env.FORK

app.use(cors())

app.get('/file', async function (req, res, next) {
    var fileName = req.query.filename
    if (typeof fileName === 'undefined') {
        res.send(`no filename given`)
        return
    }
    try {
        res.sendFile(fileName, function (err) {
            if (err) {
                next(err)
            } else {
                console.log('Sent:', fileName)
            }
            })
    } catch (error) {
        res.send(`file not found: ${fileName}`)
    }
    
  })

class FileServer {
  // 0 will result in a random open port being assigned 
  constructor(port = 0) {
    this.cliArgs = parser(process.argv, {default: {port: 0}})
    this.port = this.cliArgs.port === 0 ? port : this.cliArgs.port

  }

  start() {
    
    this.server = server.listen(this.port)
    this.port = this.server.address().port // update port reference if it was randomly assigned
		process.env.FSLGUI_FS_PORT = this.port
    if (FORK) {
      process.send({ port: this.port })
    }
    return this
  }

  quit() {
    this.server.close((err) => {
      process.exit(err ? 1 : 0)
    })
  }
}

module.exports.FileServer = FileServer

if (FORK) {
  const fileServer = new FileServer()
  fileServer.start()
}

