const ioclient = require("socket.io-client");

class SocketClient {
  constructor(port) {
    this.port = port
    this.io = ioclient(`ws://localhost:${this.port}`)
  }
}
module.exports.SocketClient = SocketClient

