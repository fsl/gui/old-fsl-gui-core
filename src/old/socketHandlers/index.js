// this is a custom loader. 
// each file in socketHandlers defines a default export function that is a 
// socket.io message handler. The filenames and the message names should be the same. 
// 
// for example, the file file.js should define the message handler for the message 'files'
require('fs').readdirSync(__dirname + '/').forEach(function(file) {
  if (file.match(/\.js$/) !== null && file !== 'index.js') {
    var name = file.replace('.js', '');
    module.exports[name] = require('./' + file);
  }
});