const {getGuiInfo, getInstalledGuis} = require('../guidir.js')
//const getInstalledGuis = require('../guidir.js')

module.exports = (io, socket) => {
  const handler = () => {
		let guiData = getGuiInfo(getInstalledGuis())
  	io.to(socket.id).emit('guiList', guiData)
  }
  socket.on('guiList', handler) 
}
