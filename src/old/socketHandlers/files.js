const {dialog} = require('electron')
const path = require('path')

const FSLDIR = process.env.FSLDIR

module.exports = (io, socket) => {
  const handler = (msg={}) => {
    // mock a returned array of files in test mode
    // since we can't control the file browser window
    // once it has been activated (and therefore can't select a file)
    console.log('GUI MODE: ', process.env.FSL_GUI_MODE)
    if (process.env.FSL_GUI_MODE === 'test'){
      io.to(socket.id).emit('files', ['test_file.nii.gz'])
      return
    }
		console.log(msg)
    dialog.showOpenDialog(null, {
      properties: ['openFile'],
			defaultPath: 'standard' in msg ? path.join(FSLDIR, 'data', 'standard') : '',
			filters: 'filters' in msg ? msg['filters'] : []
    }).then(result => {
      io.to(socket.id).emit('files', result.filePaths)
    })
  }
  socket.on('files', handler) 
}
