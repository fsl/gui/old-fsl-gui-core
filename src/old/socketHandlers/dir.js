const {dialog} = require('electron')

module.exports = (io, socket) => {
  const handler = () => {
    if (process.env.FSL_GUI_MODE === 'test'){
      io.to(socket.id).emit('dir', ['testDir'])
      return
    }
    dialog.showOpenDialog(null, {
      properties: ['openDirectory']
    }).then(result => {
      console.log(result.filePaths)
      io.to(socket.id).emit('dir', result.filePaths)
    })
  }
  socket.on('dir', handler) 
}
