const {spawn} = require('child_process')
const {path} = require('path')

module.exports = (io, socket) => {
  const handler = (data) => {
		let cmdStr = data['run']	
		cmdStr = process.env.FSLDIR + '/share/fsl/bin/' + cmdStr // TODO, allow non unix path sep
		console.log(cmdStr)
		const fslProcess = spawn(cmdStr, [], {shell: true}) // {shell: true}
		fslProcess.on('close', (status)=>{
			console.log('emitting to run')
			io.to(socket.id).emit('run', {
				'status': status,
			})
		})
		fslProcess.stdout.on('data', (data)=>{
			console.log(data.toString('utf8'))
			io.to(socket.id).emit('stdout', {
				'stdout': data.toString('utf8'),
			})
		})
		fslProcess.stderr.on('data', (data)=>{
			console.log(data.toString('utf8'))
			io.to(socket.id).emit('stderr', {
				'stderr': data.toString('utf8'),
			})
		})


  }
  socket.on('run', handler) 
}
