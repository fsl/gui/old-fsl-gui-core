const {createWindow} = require('../window')

module.exports = (io, socket) => {
  const handler = (data) => {
    createWindow(data)
  }
  socket.on('createWindow', handler) 
}
