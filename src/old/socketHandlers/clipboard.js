const {clipboard} = require('electron')

module.exports = (io, socket) => {
  const handler = (msg={}) => {
		console.log(msg)
		clipboard.writeText(msg['text'] || '')
		io.to(socket.id).emit('clipboard', {'success':true})
  }
  socket.on('clipboard', handler) 
}
