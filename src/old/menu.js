// TODO add menu options and handlers

const {Menu, shell } = require('electron')
const isMac = process.platform === 'darwin'
const {getInstalledGuis} = require('./guidir')
const Store = require('electron-store')
const { io } = require('socket.io-client')
const os = require('os');
const homeDir = os.homedir()

const store = new Store({
    cwd: homeDir,
    name: '.fslguiconf',
  })

const listGUIs = function() {
    let guis = getInstalledGuis()
    let menuGuis = []
    let clients = []
    for (let i=0; i<guis.length; i++) {
        clients.push(io(`ws://${store.get('host')}:${store.get('port-app')}`))
        menuGuis.push(
            {
                label: guis[i].name,
                click: async () => {
                    clients[i].emit('createWindow', {gui:guis[i].name})
                }
            },
        )
    }
    return menuGuis

}

const setMenu = function (appName) {
    const template = [
        // { role: 'appMenu' }
        ...(isMac ? [{
            label: appName,
            submenu: [
                { role: 'about' },
                { type: 'separator' },
                { role: 'hide' },
                { role: 'hideOthers' },
                { role: 'unhide' },
                { type: 'separator' },
                { role: 'quit' }
            ]
        }] : []),
        // { role: 'fileMenu' }
        {
            label: 'File',
            submenu: [
                isMac ? { role: 'close' } : { role: 'quit' }
            ]
        },
        // { role: 'editMenu' }
        {
            label: 'Edit',
            submenu: [
                { role: 'undo' },
                { role: 'redo' },
                { type: 'separator' },
                { role: 'cut' },
                { role: 'copy' },
                { role: 'paste' },
                ...(isMac ? [
                    { role: 'pasteAndMatchStyle' },
                    { role: 'delete' },
                    { role: 'selectAll' },
                    { type: 'separator' },
                    {
                        label: 'Speech',
                        submenu: [
                            { role: 'startSpeaking' },
                            { role: 'stopSpeaking' }
                        ]
                    }
                ] : [
                    { role: 'delete' },
                    { type: 'separator' },
                    { role: 'selectAll' }
                ])
            ]
        },
        // { role: 'viewMenu' }
        {
            label: 'View',
            submenu: [
                { role: 'reload' },
                { role: 'forceReload' },
                { role: 'toggleDevTools' },
                { type: 'separator' },
                { role: 'resetZoom' },
                { role: 'zoomIn' },
                { role: 'zoomOut' },
                { type: 'separator' },
                { role: 'togglefullscreen' }
            ]
        },
        // { role: 'windowMenu' }
        {
            label: 'Window',
            submenu: [
                {
                    label: "FSL tools",
                    submenu: listGUIs()
                },
                { role: 'minimize' },
                { role: 'zoom' },
                ...(isMac ? [
                    { type: 'separator' },
                    { role: 'front' },
                    { type: 'separator' },
                    { role: 'window' }
                ] : [
                    { role: 'close' }
                ])
            ]
        },
        {
            role: 'help',
            submenu: [
                {
                    label: 'FSL wiki',
                    click: async () => {
                        await shell.openExternal('https://fsl.fmrib.ox.ac.uk/fsl/fslwiki')
                    }
                },
                {
                    label: 'FSL email list',
                    click: async () => {
                        await shell.openExternal('http://www.jiscmail.ac.uk/lists/fsl.html')
                    }
                },
                {
                    label: 'FSL GUI sources',
                    click: async () => {
                        await shell.openExternal('https://git.fmrib.ox.ac.uk/fsl/gui')
                    }
                }
            ]
        }
    ]

    const menu = Menu.buildFromTemplate(template)
    Menu.setApplicationMenu(menu)

}

module.exports.setMenu = setMenu
