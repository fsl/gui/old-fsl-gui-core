
const { app } = require('electron');
const { fork } = require('child_process')
const { createWindow } = require('./window');
const { SocketClient } = require('./socketClient');
const { socketServer, SocketServer } = require('./socketServer')
const { parseCLI } = require('./cli.js')
const path = require('path');
const os = require('os')
const fs = require('fs')
const {spawn} = require('child_process')

// default all ports to null. Only show a window when all child process
// servers have reported their ports
process.env.FSLGUI_FS_PORT = 'undefined'
process.env.FSLGUI_WS_PORT = 'undefined'
process.env.FSLGUI_GUI_PORT = 'undefined'
process.env.FSLGUI_HOST = 'localhost'

function setFSLPackageVersionText(app){
	const versionProc = spawn(process.env.FSLDIR + '/bin/conda list --json fsl', [], {shell: true}) // {shell: true}
	let versions = ''
	versionProc.stdout.on('data', (data)=>{
		console.log(data.toString('utf8'))
		versions = data.toString('utf8')
		app.setAboutPanelOptions({'applicationVersion': versions})
	})
	versionProc.stderr.on('data', (data)=>{
		console.log(data.toString('utf8'))
	})
	
	
}

class FSL {
  constructor() {
    this.app = app
		//setFSLPackageVersionText(this.app) // TODO make into a separate About GUI window
    this.isPrimaryInstance = this.app.requestSingleInstanceLock()
    app.commandLine.appendSwitch("disable-http-cache") // chromium specific switch
    // app.commandLine.appendSwitch ("no-sandbox") 
    // disable some CORS related issues
    app.commandLine.appendSwitch('disable-features', 'OutOfBlinkCors', 'ignore-gpu-blacklist'); // chromium specific switch

    this.socketServer = null
    this.socketServerPort = null
    this.socketClient = null
    this.guiServer = null
    this.guiServerPort = null
    this.fileServer = null
    this.fileServerPort = null

    this.children = []
    this.childrenReady = 0 // count number of children ready
    this.appArgs = parseCLI(process.argv)
    // register the windows closed handler
    this.app.on('window-all-closed', () => {
      this.quit()
    })
    this.app.on('quit', () => {
      this.quit()
    })
  }

  quit() {
    this.quitSocketClient()
    this.children.forEach((child) => {
      child.kill()
    })
		// reset fslgui.env file to undefined values
		fs.writeFileSync(
			path.join(
				os.homedir(),
				'.fslgui.env'
			), 
			`VITE_FSLGUI_HOST=undefined
VITE_FSLGUI_GUI_PORT=undefined
VITE_FSLGUI_WS_PORT=undefined
VITE_FSLGUI_FS_PORT=undefined`
		)
    this.app.quit()
  }


  quitSocketClient() {
    this.socketClient = null
  }

  start() {
    // quit if new instance is not primary instance
    if (!this.isPrimaryInstance) {
      this.quit()
    } else {
      this.app.on('second-instance', (event, argv) => {
        let args = parseCLI(argv)
        //createWindow({ 'gui': args.gui })
        return
      })
    }
    // only perform the following steps if the app is being started for the first time
    if (this.isPrimaryInstance) {

      this.socketServer = new SocketServer(0).start()
      this.socketServerPort = this.socketServer.port
			process.env.FSLGUI_WS_PORT = this.socketServerPort

      this.app.on('ready', () => {
        process.env.FORK = 1
        // gui server
        this.guiServer = fork(
          path.join(__dirname, 'staticGUIServer.js'),
          [`--port=${this.appArgs["port-gui-server"]}`],
          { env: process.env }
        )
        this.children.push(this.guiServer)

        // file server
        this.fileServer = fork(
          path.join(__dirname, 'fileServer.js'),
          [`--port=0`],
          { env: process.env }
        )
        this.children.push(this.fileServer)

        // set gui server port
        this.guiServer.on('message', (msg) => {
          if (msg.port) {
            this.guiPort = msg.port
						process.env.FSLGUI_GUI_PORT = this.guiPort
						// only create window when all ports have been established. This is async
						if (
							process.env.FSLGUI_GUI_PORT !== 'undefined' &&
							process.env.FSLGUI_WS_PORT  !== 'undefined' &&
							process.env.FSLGUI_FS_PORT  !== 'undefined'
						){
							createWindow({ 
								'gui': this.appArgs.gui,
								'host': this.appArgs.host,
								'webSocketPort': this.webSocketPort,
							})
							console.log('FSLGUI_HOST     ', process.env.FSLGUI_HOST)
							console.log('FSLGUI_FS_PORT  ', process.env.FSLGUI_FS_PORT)
							console.log('FSLGUI_WS_PORT  ', process.env.FSLGUI_WS_PORT)
							console.log('FSLGUI_GUI_PORT ', process.env.FSLGUI_GUI_PORT)
							fs.writeFileSync(
								path.join(
									os.homedir(),
									'.fslgui.env'
								), 
								`VITE_FSLGUI_HOST=${process.env.FSLGUI_HOST}
VITE_FSLGUI_GUI_PORT=${process.env.FSLGUI_GUI_PORT}
VITE_FSLGUI_WS_PORT=${process.env.FSLGUI_WS_PORT}
VITE_FSLGUI_FS_PORT=${process.env.FSLGUI_FS_PORT}`
							)
						}
          }
        })
        // set file server port
        this.fileServer.on('message', (msg) => {
          if (msg.port) {
            this.fileServerPort = msg.port
						process.env.FSLGUI_FS_PORT = this.fileServerPort
						// only create window when all ports have been established. This is async
						if (
							process.env.FSLGUI_GUI_PORT !== 'undefined' &&
							process.env.FSLGUI_WS_PORT  !== 'undefined' &&
							process.env.FSLGUI_FS_PORT  !== 'undefined'
						){
							createWindow({ 
								'gui': this.appArgs.gui,
								'host': this.appArgs.host,
								'webSocketPort': this.webSocketPort,
							})
							console.log('FSLGUI_HOST     ', process.env.FSLGUI_HOST)
							console.log('FSLGUI_FS_PORT  ', process.env.FSLGUI_FS_PORT)
							console.log('FSLGUI_WS_PORT  ', process.env.FSLGUI_WS_PORT)
							console.log('FSLGUI_GUI_PORT ', process.env.FSLGUI_GUI_PORT)
							fs.writeFileSync(
								path.join(
									os.homedir(),
									'.fslgui.env'
								), 
								`VITE_FSLGUI_HOST=${process.env.FSLGUI_HOST}
VITE_FSLGUI_GUI_PORT=${process.env.FSLGUI_GUI_PORT}
VITE_FSLGUI_WS_PORT=${process.env.FSLGUI_WS_PORT}
VITE_FSLGUI_FS_PORT=${process.env.FSLGUI_FS_PORT}`
							)
						}
          }
        })

      })

    }
  }
}
module.exports.FSL = FSL

const fsl = new FSL()
fsl.start()
