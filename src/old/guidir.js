/**
 * @author Taylor Hanayik <taylor.hanayik@ndcn.ox.ac.uk>
 * @namespace guidir
 * @description
 * Helper functions to read and parse info about installed FSL GUIs
 */
const fs = require('fs')
const path = require('path')

/**
 * @memberof guidir
 * @description
 * default guidir search location is the ENV variable $FSLGUIDIR
 * ```
 * # example
 * export FSLGUIDIR=$FSLDIR/gui
 * ```
 * @param {string} [guidir=$FSLGUIDIR] - the path to search for installed gui
 * @example getInstalledGuis(guidir)
 */
function getInstalledGuis(guidir=process.env.FSLGUIDIR) {
  let guis = []
  if (guidir === undefined || !fs.existsSync(guidir)){
    return guis // return empty array if guidir is not defined
  }
fs.readdirSync(guidir).filter(function (item) {
    if (fs.statSync(path.join(guidir, item)).isDirectory()){
      guis.push( {
        name: item,
        fullPath: path.join(guidir, item)
      })
    }
  })
  return guis
}

/**
 * @memberof guidir
 * @description
 * parse GUI info from all installed GUIs
 * @param {array} installedGuiList - the array of installed GUIs returned from getInstalledGUIs
 * @example getGuiInfo(installedGuiList)
 */
function getGuiInfo(installedGuiList) {
  let guiInfo = []
  installedGuiList.forEach(function (item) {
    let metaPath = path.join(item.fullPath, 'meta.json')
    if (fs.existsSync(metaPath)){
      let metaRaw = fs.readFileSync(metaPath)
      let metaJson = JSON.parse(metaRaw)
      guiInfo.push(metaJson)
    }
  })
  return guiInfo
}

exports.getGuiInfo = getGuiInfo
exports.getInstalledGuis = getInstalledGuis
