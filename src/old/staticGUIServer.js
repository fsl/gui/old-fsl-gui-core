const express = require('express');
const { mkdirSync, existsSync} = require('fs');
const app = express()
const server = require('http').createServer(app);
const parser = require('minimist');
const { homedir } = require('os');
const path = require('path');
const FORK = process.env.FORK

class GUIServer {
  // 0 will result in a random open port being assigned 
  constructor(port = 0) {
    this.cliArgs = parser(process.argv, {default: {port: 0}})
    this.port = this.cliArgs.port === 0 ? port : this.cliArgs.port

  }

  start() {
    let staticFilePath = process.env.FSLGUIDIR
    if (typeof staticFilePath == 'undefined') {
      staticFilePath = path.join(homedir(),'.fslgui')
      if (!existsSync(staticFilePath)) {
        mkdirSync(staticFilePath)
      }
      // throw new Error('static file path is required. Please set $FSLGUIDIR')
    }
		app.use('/gui', express.static(staticFilePath))
    this.server = server.listen(this.port)
    this.port = this.server.address().port // update port reference if it was randomly assigned
		process.env.FSLGUI_GUI_PORT = this.port
    if (FORK) {
      process.send({ port: this.port })
    }
    return this
  }

  quit() {
    this.server.close((err) => {
      process.exit(err ? 1 : 0)
    })
  }
}

module.exports.GUIServer = GUIServer

if (FORK) {
  const guiServer = new GUIServer()
  guiServer.start()
}

