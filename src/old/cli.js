/**
 * @author Taylor Hanayik <taylor.hanayik@ndcn.ox.ac.uk>
 * @namespace cli
 * @description
 * Parses command line flags given to the app via the terminal.
  ```
  known flags are:
    --gui     [fsl] (e.g. bet, flirt, feat). Default is fsl
    --g       alias for --gui
    --host    [localhost] The host of the app TCP socket server. Default is localhost
    --h       alias for --host
    --port    [5000] The TCP port that the app connects to. Default is 5000
    --p       alias for --port
  ```
 *
 *
 */

let parser = require('minimist')

let argOpts = {
  default:{
    gui: 'dashboard', // default to fsl startup (tool dashboard)
    "port-app": 0, // application web socket port (controls electron instance)
    "port-gui-server": 0,
    "port-image-server": 0,
    host: 'localhost' // default appSocketHost
  }
}

/**
 * Parse all command line flags given by argv
 * @memberof cli
 * @param {array} argv - the array of command line flags given to the app
 * @example
 * args = parseCLI(['--gui', 'bet',]) // returns args object with argOpts properties set by known flags
 */
function parseCLI(argv) {
  return parser(argv, argOpts)
}

exports.parseCLI = parseCLI
