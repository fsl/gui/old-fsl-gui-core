const { BrowserWindow, globalShortcut} = require('electron')
const {setMenu} = require('./menu')
// createWindow shows a new window with the requested GUI
// gui: string
let createWindow  = function(options={}) {
  let show = process.env.FSL_GUI_MODE === 'test' ? false : true
  const win = new BrowserWindow({
    width: 1000,
    height: 800,
    show: show,
    webPreferences: {
      devTools: true,
      nodeIntegration: true, 
      enableRemoteModule: true,
      webSecurity: false,
      allowRunningInsecureContent: true,
      plugins: true,
      contextIsolation: false,
      backgroundThrottling: false,
      experimentalFeatures: true
    }
  })
  let gui = options.gui || 'dashboard' // show dashboard by default
	let host = options.host || 'localhost'
	let socketServerPort = options.socketServerPort || null
	let fileServerPort = options.fileServerPort || null
  win.setTitle(gui)

  if (process.env.FSL_GUI_MODE === 'test'){
    win.showInactive() // dont focus on the window when it appears on screen
  }

  /*
	globalShortcut.register('CommandOrControl+R', function() {
		win.reload()
	})
	*/

	console.log(`http://${process.env.FSLGUI_HOST}:${process.env.FSLGUI_GUI_PORT}/gui/${gui}?host=${host}&socketServerPort=${process.env.FSLGUI_WS_PORT}&fileServerPort=${process.env.FSLGUI_FS_PORT}`)

  win.loadURL(`http://${process.env.FSLGUI_HOST}:${process.env.FSLGUI_GUI_PORT}/gui/${gui}?host=${host}&socketServerPort=${process.env.FSLGUI_WS_PORT}&fileServerPort=${process.env.FSLGUI_FS_PORT}`)


  setMenu('FSL')


}
// export creatWindow
module.exports.createWindow = createWindow
