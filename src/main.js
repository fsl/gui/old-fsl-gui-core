const {
  app,
  BrowserWindow,
  Menu,
  MenuItem,
  systemPreferences,
  dialog,
} = require("electron");
const path = require("path");
const { fork, spawn } = require("child_process");
const fs = require("fs");

let win;
let appMenuDefinition;
const backgroundServices = [];
let fileServerPort = null;
let socketServerPort = null;
let socketClientID = null;
let fileServer = {};
let socketServer = {};

const isMac = process.platform === "darwin";
if (isMac) {
  systemPreferences.setUserDefault(
    "NSDisabledDictationMenuItem",
    "boolean",
    true
  );
  systemPreferences.setUserDefault(
    "NSDisabledCharacterPaletteMenuItem",
    "boolean",
    true
  );
}

function onFileServerPort(port) {
  fileServerPort = port;
  // only create window when all subprocesses have been established
  if (fileServerPort !== null && socketServerPort !== null) {
    // if the server dependencies are ready, show the browser window
    if (BrowserWindow.getAllWindows().length === 0) {
      createWindow({
        fileServerPort: fileServerPort,
        socketServerPort: socketServerPort,
      });
    }
  }
}

function onSocketServerPort(port) {
  socketServerPort = port;
  // only create window when all subprocesses have been established
  if (fileServerPort !== null && socketServerPort !== null) {
    // if the server dependencies are ready, show the browser window
    if (BrowserWindow.getAllWindows().length === 0) {
      createWindow({
        fileServerPort: fileServerPort,
        socketServerPort: socketServerPort,
      });
    }
  }
}

function onExampleMessage(message) {
  /*
  a message sent to the socketServer process must have the form:
  {
    type: 'string',
    socketID: 'string', // from sender
    value: Object
  }
  */
  socketServer.send({
    type: message.type,
    socketID: message.socketID,
    value: "someValue",
  });
}

// list all directories in FSLGUIDIR
function listDirectories(dir) {
  return fs
    .readdirSync(dir, { withFileTypes: true })
    .filter((dirent) => dirent.isDirectory())
    .map((dirent) => dirent.name);
}

function getGuiInfo(installedGuis){
  let guiInfo = []
  installedGuis.forEach((gui)=>{
    let guiPath = path.join(process.env.FSLGUIDIR, gui)
    let guiJsonPath = path.join(guiPath, 'meta.json')
    if (!fs.existsSync(guiJsonPath)){
      console.log(`meta.json not found for ${gui}`)
    } else {
      let guiJson = JSON.parse(fs.readFileSync(guiJsonPath, 'utf8'))
      if (gui !== 'dashboard'){
        guiInfo.push(guiJson)
      }
    }
  })
  return guiInfo
}

function onGuiList(){
  let installedGuis = listDirectories(process.env.FSLGUIDIR)
  let guiInfo = getGuiInfo(installedGuis)
  socketServer.send({
    type: 'guiList',
    socketID: socketClientID,
    value: guiInfo
  })
}

function onFile() {
  dialog.showOpenDialog(win, {
    properties: ["openFile"],
    filters: [
      { name: 'volumes', extensions: ['nii.gz', 'nii'] }
    ]
  }).then(result => {
    if (result.canceled === true) {
      socketServer.send({
        type: 'file',
        socketID: socketClientID,
        value: null
      })
    } else {
        socketServer.send({
          type: 'file',
          socketID: socketClientID,
          value: result.filePaths[0]
        })
    }
  });
}

function onRun(command) {
  cmdString = path.join(
    process.env.FSLDIR,
    "bin",
    command
  )
  console.log(cmdString)
  const fslProcess = spawn(cmdString, [], {shell: true}) // {shell: true}
  fslProcess.on('close', (status)=>{
    console.log('emitting to run')
    socketServer.send(
      {
        type: 'run',
        socketID: socketClientID,
        value: status
      }
    )
  })
  fslProcess.stdout.on('data', (data)=>{
    console.log(data.toString('utf8'))
    socketServer.send(
      {
        type: 'run',
        socketID: socketClientID,
        value: {'stdout': data.toString('utf8')}
      }
    )
  })
  fslProcess.stderr.on('data', (data)=>{
    console.log(data.toString('utf8'))
    socketServer.send(
      {
        type: 'run',
        socketID: socketClientID,
        value: {'stderr': data.toString('utf8')}
      }
    )
  })

  // cmdStr = process.env.FSLDIR + '/share/fsl/bin/' + cmdStr // TODO, allow non unix path sep
}

function handleFileServerMessage(message) {
  // msg is expected to be a JSON object (automatically serialized and deserialized by process.send and 'message')
  if (message.type === "port") {
    onFileServerPort(message.value);
  }
}

function handleSocketServerMessage(message) {
  // msg is expected to be a JSON object (automatically serialized and deserialized by process.send and 'message')
  socketClientID = message.socketID;
  switch (message.type) {
    case "port":
      onSocketServerPort(message.value);
      break;
    case "file":
      onFile()
      break;
    case "run":
      onRun(message.value)
      break;
    case "guiList":
      onGuiList()
      break;
    case "createWindow":
      createWindow({
        gui: message.value.gui,
        fileServerPort: fileServerPort,
        socketServerPort: socketServerPort
      })
      break;
    default:
      console.log("unsupported message", message.type);
  }
}

// launch the fileServer as a background process
fileServer = fork(
  path.join(__dirname, "fileServer.js"),
  [`--port=0`, `--host=${"localhost"}`],
  { env: { FORK: true } }
);
// launch the socketServer as a background process
socketServer = fork(
  path.join(__dirname, "socketServer.js"),
  [`--port=0`, `--host=${"localhost"}`],
  { env: { FORK: true } }
);

// add both file server and socket server to our list of open background services (for exiting later)
backgroundServices.push(fileServer);
backgroundServices.push(socketServer);

fileServer.on("message", (message) => {
  handleFileServerMessage(message);
});

socketServer.on("message", (message) => {
  handleSocketServerMessage(message);
});

function createWindow(config = {}) {
  win = new BrowserWindow({
    width: 600,
    height: 600,
    title: "FSL",
    show: true,
  });
  if (config.gui === undefined){
    config.gui = 'dashboard'
  }
  // win.webContents.openDevTools();
  let url = `http://localhost:${config.fileServerPort}/gui/${config.gui}/?socketServerPort=${config.socketServerPort}&fileServerPort=${config.fileServerPort}`
  if (process.env.FSL_DEV_LAUNCH_DELAY) {
    //   let url = `http://localhost:8080?fileServerPort=${config.fileServerPort}&socketServerPort=${config.socketServerPort}`;
    setTimeout(() => {
      win.loadURL(url);
    }, Number(process.env.FSL_DEV_LAUNCH_DELAY));
  } else {
    //   let url = `http://localhost:${fileServerPort}/gui/index.html?fileServerPort=${config.fileServerPort}&socketServerPort=${config.socketServerPort}`;
    win.loadURL(url);
  }
}

app.on("window-all-closed", () => {
  backgroundServices.forEach((service) => {
    service.kill();
  });
  app.quit();
});

appMenuDefinition = [
  ...(isMac
    ? [
      {
        label: "FSL",
        submenu: [
          {
            label: "About",
            click: () => {
              // do nothing for now. Eventually show a dialog here.
            },
          },
          { type: "separator" },
          { role: "services" },
          { type: "separator" },
          { role: "hide" },
          { role: "hideOthers" },
          { role: "unhide" },
          { type: "separator" },
          { role: "quit" },
        ],
      },
    ]
    : []),
  // Window menu
  {
    label: "Window",
    submenu: [
      { role: "minimize" },
      { role: "zoom" },
      { role: 'reload' },
      { role: 'forceReload' },
      ...(isMac
        ? [
          { type: "separator" },
          { role: "front" },
          { type: "separator" },
          { role: "window" },
        ]
        : [{ role: "close" }]),
    ],
  },
  //Develop menu
  {
    label: "Develop",
    submenu: [
      {
        label: "Tools",
        role: "toggleDevTools", //openDevTools, closeDevTools
        // onClick: () => {
        //   console.log("clicked");
        //   // win.webContents.openDevTools();
        // }
      },
    ],
  },
];


const menu = Menu.buildFromTemplate(appMenuDefinition);

app.whenReady().then(() => {
  Menu.setApplicationMenu(menu);
  if (
    BrowserWindow.getAllWindows().length === 0 &&
    fileServerPort !== null &&
    socketServerPort !== null
  ) {
    createWindow({
      fileServerPort: fileServerPort,
      socketServerPort: socketServerPort,
    });
  }
  app.on("activate", () => {
    if (
      BrowserWindow.getAllWindows().length === 0 &&
      fileServerPort !== null &&
      socketServerPort !== null
    ) {
      createWindow({
        fileServerPort: fileServerPort,
        socketServerPort: socketServerPort,
      });
    }
  });
});
