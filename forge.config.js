module.exports = {
  packagerConfig: {
    ignore: "(.git|.vscode|docs|dist|.ci|screenshots|tests|node_modules|.gitignore|README.md|LICENSE.md)",
    icon: "./icon",
    osxSign: {},
  },
  rebuildConfig: {},
  makers: [
    {
      name: '@electron-forge/maker-squirrel',
      config: {},
    },
    {
      name: '@electron-forge/maker-zip',
      platforms: ['darwin', 'linux'],
    },
    {
      name: '@electron-forge/maker-deb',
      config: {},
    },
    {
      name: '@electron-forge/maker-rpm',
      config: {},
    },
  ],
};
