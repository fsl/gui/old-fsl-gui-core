#!/bin/bash

set -e

# Install system dependencies
apt-get update  -y
apt-get install -y --ignore-missing \
  software-properties-common \
  libx11-xcb1 \
  xvfb \
  bzip2 \
  wget \
  rsync \
  git \
  libxcb-dri3-0 \
  libxtst6 \
  libnss3 \
  libatk-bridge2.0-0 \
  libgtk-3-0 \
  libxss1 \
  libasound2 \
  libgbm-dev \
  x11-xserver-utils

apt install -y locales
locale-gen en_US.UTF-8
locale-gen en_GB.UTF-8
update-locale
